package com.example.ATM;

import com.example.ATM.dao.TransferService;
import com.example.ATM.dao.UserDAO;
import com.example.ATM.dao.UserService;
import com.example.ATM.entity.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class AtmApplication {

	public static void main(String[] args) {
		SpringApplication.run(AtmApplication.class, args);
	}



	}
