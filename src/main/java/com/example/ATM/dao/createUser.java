package com.example.ATM.dao;

import com.example.ATM.dao.UserDAO;
import com.example.ATM.entity.User;

public class createUser {

    private UserDAO userDAO;

    public createUser(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public void addUser(String username, String password, String firstName, String lastName, double balance) {
        User tempUser = new User(username, password, firstName, lastName, balance);
        userDAO.save(tempUser);
    }

}
