package com.example.ATM.dao;

import com.example.ATM.entity.User;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserDAOImpl implements UserDAO{

    private EntityManager entityManager;

    @Autowired
    public UserDAOImpl(EntityManager entityManager) {this.entityManager = entityManager;}


    @Override
    @Transactional
    public void save(User theUser) {
        entityManager.persist(theUser);
    }
}
