package com.example.ATM.dao;

import com.example.ATM.entity.User;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.swing.*;

@Service
public class TransferService {

    private UserRepository userRepository;

    @Autowired
    public TransferService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Transactional
    public boolean transferFunds(int senderUserId, int recipientUserId, double amount) {
        User sender = userRepository.findById(senderUserId).orElse(null);
        User recipient = userRepository.findById(recipientUserId).orElse(null);

        if (sender == null || recipient == null) {
            JOptionPane.showMessageDialog(null, "User not found.", "Error", JOptionPane.ERROR_MESSAGE);
        }

        if (sender.getBalance() < amount) {
            JOptionPane.showMessageDialog(null, "Insufficient balance.", "Error", JOptionPane.ERROR_MESSAGE);
        }

        sender.setBalance(sender.getBalance() - amount);
        recipient.setBalance(recipient.getBalance() + amount);


        userRepository.save(sender);
        userRepository.save(recipient);

        JOptionPane.showMessageDialog(null, "Funds transfer has been successful!", "Success", JOptionPane.INFORMATION_MESSAGE);
        return false;
    }
}
