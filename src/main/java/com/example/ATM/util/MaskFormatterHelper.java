package com.example.ATM.util;

import javax.swing.text.MaskFormatter;
import java.text.ParseException;
import java.util.Optional;

public class MaskFormatterHelper {
    public static Optional<MaskFormatter> createPasswordFormatter() {
        try {
            MaskFormatter formatter = new MaskFormatter("#######");
            return Optional.of(formatter);
        } catch (ParseException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }
}
